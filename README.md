# Google Files System

- From the paper:    [The Google File System](https://static.googleusercontent.com/media/research.google.com/en//archive/gfs-sosp2003.pdf)
  - GFS Architecture:
    - A single master
    - Multiple chunkservers (acessed by multiple clients)
      - Files are divided into fixed-size chunks
      - Each chunk is replicated on multiple chunkservers
    - The master maintains all file system metadata
    - Chunkservers need not cache file data because chunks are stored as local files and so Linux’s buffer cache already keeps frequently accessed data in memory.
    - A client asks the master which chunkservers it should contact.

![](figures/moosefs-pro-architecture.png)
**Figure 1**: GFS Architecture  

  - **First**, using the fixed chunksize, the client translates the file name and byte offset specified by the application into a chunk index within the file. Then, it sends the master a request containing the file name and chunk index.
    - **Chunksize** is one of the key design parameters. We have chosen 64 MB, which is much larger than typical file system blocksizes.
  - **Second**, since on a large chunk, a client is more likely to perform many operations on a given chunk, it can reduce network overhead by keeping a persistent TCP (Transmission Control Protocol) connection to the chunkserver over an extended period of time.
  - **Third**, it reduces the size of the metadata stored on the master.
  
  - **Metadata** master stores three major types of:
    - All metadata is kept in the master’s memory
      - The file and chunck namespaces
      - The mapping from files to chunks
      - The locations of each chunck's replicas
    - It is easy and efficient for the master to
periodically scan through its entire state in the background
  - The master does not keep a persistent record of which
chunkservers have a replica of a given chunk. It simply polls
chunkservers for that information at startup. The master
can keep itself up-to-date thereafter because it controls all
chunk placement and monitors chunkserver status with regular **HeartBeat messages**.
  - The operation log contains a historical record of critical metadata changes.

```bash
# Google File System (GFS):
# I will explain how to run this example in details
# example from github: https://github.com/looi/CS244B 
# All right reserved to the authors
# This is just to learn about GFS.

# Open a terminal and type:
$ cd
$ cd GFS
$ git submodule update --init # clones grpc submodule
$ cd grpc # origin/master hash: 09ace01cc120e514b33c986bd557c72c80496bad (2020-10-15)
$ git submodule update --init # clones grpc dependencies
# install openssl
# see tutorial to do it: https://cloudwafer.com/blog/installing-openssl-on-ubuntu-16-04-18-04/
# sudo apt-get install -y autoconf  (maybe if something go wrong)
$ make                        # builds grpc
$ cd third_party/zlib
$ ./configure
$ make                    # builds zlib
$ cd ../../..             # back to CS244B root folder
$ make                    # builds CS244B project
```

If everything goes well, now you will to do the GFS:
  - master
  - chunckservers
  - clients

### Run master

```bash
# Open a terminal and type:
# bin/gfs_master <path to sqlite database> master_address (like IP:port)
$ bin/gfs_master chinook.db 127.0.0.1:50052
Server listening on 127.0.0.1:50052
```

### Run chunkservers:

For this example, I will create only four chunkservers, if you want more, feel free to do! Now you will connect the chunckservers (**PORT:11111**, **22222**, **33333** and **4444**) to master:

```bash
# Open another terminal and type:
# bin/gfs_server master_address (like IP:port  <path_to_local_file_directory> chunkserver_address (like IP:port)
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:44444

# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:33333

# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:22222

# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:11111
```

After this command above, you will see in the terminal where are location the 'master':

```bash
# master
$ bin/gfs_master chinook.db 127.0.0.1:50052
Server listening on 127.0.0.1:50052
Server listening on 127.0.0.1:50052
Found out about new chunkserver: 127.0.0.1:44444
Found out about new chunkserver: 127.0.0.1:33333
Found out that chunkserver 127.0.0.1:33333 stores chunk 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
ERROR: Number of known chunkservers is less than 3
chunkhandle[1] servers changed from: 1 to: 1
Found out about new chunkserver: 127.0.0.1:22222
Found out that chunkserver 127.0.0.1:22222 stores chunk 1
Added new location 127.0.0.1:44444 for chunkhandle 1
chunkhandle[1] servers changed from: 2 to: 3
ReplicateChunks request from: 127.0.0.1:33333 to: 127.0.0.1:44444: OK
Found out about new chunkserver: 127.0.0.1:11111
Found out that chunkserver 127.0.0.1:11111 stores chunk 1
Found out that chunkserver 127.0.0.1:11111 stores chunk 4
Chunkhandle 4 no longer exists.
DeleteChunks request to 127.0.0.1:11111: (12: Not implemented.)
Created new chunkhandle 2
Added new location 127.0.0.1:22222 for chunkhandle 2
Added new location 127.0.0.1:33333 for chunkhandle 2
Added new location 127.0.0.1:11111 for chunkhandle 2

```

and

```bash
# Open another terminal and type:
# bin/gfs_server master_address (like IP:port  <path_to_local_file_directory> chunkserver_address (like IP:port)
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:44444      
Successfully registered with master.
There is no metadata file for reading: /home/andsilva/repo/acesso-multiple-readers-writers/GFS/metadata127.0.0.1:44444
Server listening on 127.0.0.1:44444
chunkhandle metadata: 1
Got server PushData for clientid = 42
chunkhandle metadata: 1


# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:33333
Successfully registered with master.
chunkhandle metadata: 1
Server listening on 127.0.0.1:33333
Got replication request to location: 127.0.0.1:44444
added chunk_info for 1
Copy request succeeded from: 127.0.0.1:33333 to: 127.0.0.1:44444
Got server PushData for clientid = 42
Got server WriteChunk for chunkhandle = 1
CS location: 127.0.0.1:22222
SerializedWrite bytes_written = 5 at location: 127.0.0.1:22222
CS location: 127.0.0.1:44444
SerializedWrite bytes_written = 5 at location: 127.0.0.1:44444
CS location: 127.0.0.1:11111
SerializedWrite bytes_written = 5 at location: 127.0.0.1:11111
chunkhandle metadata: 1
Got server PushData for clientid = 42
chunkhandle metadata: 2


# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:22222
Successfully registered with master.
chunkhandle metadata: 1
Server listening on 127.0.0.1:22222
Got server PushData for clientid = 42
chunkhandle metadata: 1
Got server PushData for clientid = 42
Got server WriteChunk for chunkhandle = 2
CS location: 127.0.0.1:33333
SerializedWrite bytes_written = 5 at location: 127.0.0.1:33333
CS location: 127.0.0.1:11111
SerializedWrite bytes_written = 5 at location: 127.0.0.1:11111
chunkhandle metadata: 2


# and another terminal type:
$ bin/gfs_server 127.0.0.1:50052 $PWD 127.0.0.1:11111
Successfully registered with master.
chunkhandle metadata: 1
chunkhandle metadata: 4
Server listening on 127.0.0.1:11111
Got DeleteChunks request for 4
Got server PushData for clientid = 42
chunkhandle metadata: 1
Got server PushData for clientid = 42
chunkhandle metadata: 2
```

The figure below shows the master and chunkservers in the terminal, you can see that master found the chunkservers.

![](figures/MasterChunks.png)
**Figure 2**: Master and four chunkservers (terminal) 

### Run client
With server running, in another terminal, run: 

```bash
# bin/gfs_client master_address bm_address -m COMMAND
$ bin/gfs_client 127.0.0.1:50052 127.0.0.1:11111 -m COMMAND
Running client in COMMAND mode
Client initialized with id- 42
Usage: <command> <arg1> <arg2> <arg3>...
Options:
	read	<filepath>	<offset>	<length>
	write	<filepath>	<offset>	<data>
	append	<filepath>	<data>
	ls	<prefix>
	mv	<filepath>	<new_filepath>
	rm	<filepath>
	quit
> Write Chunk written_bytes = 5
Write status: OK
> write test/abc.txt 0 abcde
Write Chunk written_bytes = 5
Write status: OK
> ls
FindMatchingFiles results: 2 files
=======================================
test/abc.txt
test/blah.txt
```

### Git commands

```
### Basic commands to start to work with git version control:

# clone the repository in own local machine
$ git clone https://gitlab.com/andsilvadrcc/acesso-multiple-readers-writers.git

cd acesso-multiple-readers-writers

# shows the modifications of the files and folder
$ git status

# add a new file.
$ git add new_file

# add ALL to change to repository
# just do
$ git add . 

# commit the changes to the repository
# flag '-m' is for message, talk about the change you make
$ git commit -m "Add a new file"

# get the currenty version of the project from the repository
$ git pull origin master

# send the change to repository
$ git push origin master

# SSH Keys
SSH keys allow you to establish a secure connection between your computer and GitLab.
To use Private/Public key

# First of all, you need to generate the private and
# public key, using the command below:
# email associated to gitlab

$ ssh-keygen -t rsa -b 4096 -C "email@...com" 

# press enter some times
$ cat ~/.ssh/id_rsa.pub # print the public key

# copy and Paste your public 'SSH key' to gitlab:
# This will allow you pull/push without use a password.

# In the Folder repository local machine, you need use the 
# command below to set url to remote repository.

$ git remote set-url origin git@gitlab.com:andsilvadrcc acesso-multiple-readers-writers.git

# command to save the local change in your machine
# you can use the command:
# If you did some change and not yet ready
# to commit to remote repository.
$ git stash
```



# Useful Resources

 - [Understanding the Basic Commands of GIT](https://medium.com/devops-dudes/understanding-the-basic-commands-of-git-fb321e1575ea)
 - [Multiple-Client-Server-Program-in-C-using-fork](https://github.com/nikhilroxtomar/Multiple-Client-Server-Program-in-C-using-fork)

 - [Pasta de Materiais da Aula](https://drive.google.com/drive/folders/1d5ghdYbNT7PdDfqiiNqCKDQpWO52M_TZ)

- [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/).
- [How to create your SSH Keys](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html).






